## LandSat Check Modification

This is a modification of this [PoC](https://repl.it/@xowap/landsat-check)

NASA api is currently down due to the depreciation of another api it depended on, therefore I have decided to look for an alternative:

[GIBS](https://wiki.earthdata.nasa.gov/display/GIBS/GIBS+API+for+Developers) API: seems to do the trick although it has some drawbacks:

- There was no python client to access the API, a replacement of it has been implemented under *utils/gibs*.

- They use tile row/column to select the shot instead longitude/latitude: a lat/long converter to tiles has been implemented too.

- It lacks from a cloud score API therefore we will be presenting clouds to the user.


### Requirements

Install **requirement.txt** by running:

``pip install -r requirement.txt``

under your preferred virtual environment.

### Run

You can run the bot by typing the next command under the root directory of the project:

```python run.py```

## TODOs:

- Because of the clouds problems a good idea could be to provide a 3rd answer ("Unknown") to the user that way the bisection algorithm could skip those images.

- If the intention is to find out the wildfire apparitions within 5 iteration the days range should be lowered to 32 days (2^5=32), another way of achieving this is increasing the error margin of the bisection algorithm itself.

- For better identification of wildfires from the user this PoC should always display the same season. 
    
- It would be interesting to combine the GIBS API with this [Wildfire API](https://eonet.sci.gsfc.nasa.gov/api/v2.1/categories/8) for displaying different wildfires every time 
